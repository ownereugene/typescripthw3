import { v4 as uuidv4 } from 'uuid'

enum CurrencyEnum {
    USD = 'USD',
    UAH = 'UAH'
}



class Transaction{ 
    id: string;
    amount: number;
    currency: CurrencyEnum;
    constructor(amount: number, currency: CurrencyEnum) {
        this.id = uuidv4(); 
        this.amount = amount;
        this.currency = currency;
      }
}

interface ICard {
    AddTransaction(transaction:Transaction): string,
    AddTransactionWithUSD(currency: CurrencyEnum, amount: number): string,
    GetBalance(currency:CurrencyEnum): number,
    GetTransaction(id: string):Transaction | undefined
}

class Card implements ICard{
    private _transactions: Transaction[] = []
    public AddTransaction = (transaction:Transaction) =>{
        this._transactions.push(transaction);
        return transaction.id
    }
    public AddTransactionWithUSD = (currency: CurrencyEnum, amount: number) => {
        const transaction = new Transaction(amount, currency);
        this._transactions.push(transaction);
        return transaction.id;
    }
    public GetTransaction(id: string): Transaction | undefined {
        return this._transactions.find((transaction) => transaction.id === id);
      }
    public GetBalance = (currency: CurrencyEnum) => {
        let sum:number = 0
        this._transactions.forEach((transactionItem) => {
            if (transactionItem.currency === currency){
                sum += transactionItem.amount
            }
        })
        return sum
    }
}

const newTransaction = new Transaction(50, CurrencyEnum.USD)


const card = new Card();

const transactionId1 = card.AddTransactionWithUSD(CurrencyEnum.USD, 100);
const transactionId2 = card.AddTransactionWithUSD(CurrencyEnum.UAH, 200);
const transactionId3 = card.AddTransaction(new Transaction(50, CurrencyEnum.USD));

console.log(card.GetTransaction(transactionId1));
console.log(card.GetTransaction(transactionId2));
console.log(card.GetTransaction(transactionId3));

console.log('Balance in USD:', card.GetBalance(CurrencyEnum.USD));
console.log('Balance in UAH:', card.GetBalance(CurrencyEnum.UAH));