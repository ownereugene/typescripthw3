import { v4 as uuidv4 } from 'uuid';

enum CurrencyEnum {
  USD = "USD",
  UAH = "UAH",
}

interface ICard {
  AddTransaction(transaction: Transaction): void;
  GetTransaction(id: string): Transaction | undefined;
  GetBalance(currency: CurrencyEnum): number;
}

function AmountLabel(target: any, propertyKey: string): void {
  let _val: string;

  const getter = function (this: Transaction) {
    return `${_val}${this.currency}`;
  };

  const setter = function (this: Transaction, newVal: number) {
    _val = newVal.toString();
  };

  Object.defineProperty(target, propertyKey, {
    get: getter,
    set: setter,
    enumerable: true,
    configurable: true,
  });
}

class Transaction {
  public readonly id: string;
  public readonly amount: number;
  public readonly currency: CurrencyEnum;

  private _amountLabel: string;

  constructor(amount: number, currency: CurrencyEnum) {
    this.id = uuidv4();
    this.amount = amount;
    this.currency = currency;
    this._amountLabel = `${amount}${currency}`;
  }

  public get AmountLabel(): string {
    return this._amountLabel;
  }
}

class Card implements ICard {
  private transactions: Transaction[] = [];

  public AddTransaction(transaction: Transaction): void {
    this.transactions.push(transaction);
  }

  public GetTransaction(id: string): Transaction | undefined {
    for (const transaction of this.transactions) {
      if (transaction.id === id) {
        return transaction;
      }
    }
    return undefined;
  }

  public GetBalance(currency: CurrencyEnum): number {
    return this.transactions
      .filter((transaction) => transaction.currency === currency)
      .reduce((total, transaction) => total + transaction.amount, 0);
  }

  public SaveState(): CardMemento {
    const state = new MementoCardState([...this.transactions]);
    return new CardMemento(state);
  }

  public Revert(memento: CardMemento): void {
    this.transactions = memento.getState().getTransactions();
  }
}

class BonusCard extends Card {
  public AddTransaction(transaction: Transaction): void {
    super.AddTransaction(transaction);

    const bonusAmount = transaction.amount * 0.1;
    const bonusTransaction = new Transaction(bonusAmount, transaction.currency);
    super.AddTransaction(bonusTransaction);
  }
}

class MementoCardState {
  constructor(private readonly transactions: Transaction[]) {}

  public getTransactions(): Transaction[] {
    return this.transactions;
  }
}

class CardMemento {
  private readonly state: MementoCardState;

  constructor(state: MementoCardState) {
    this.state = state;
  }

  public getState(): MementoCardState {
    return this.state;
  }
}

function CardProxyLogger<T extends ICard>(card: T): T {
  const proxy = new Proxy(card, {
    get(target, property, receiver) {
      const originalMethod = (target as any)[property];
      if (typeof originalMethod === "function") {
        return function (...args: any[]) {
          console.log(
            `Calling method ${String(property)} with arguments:`,
            args
          );
          const result = originalMethod.apply(this, args);
          console.log(`Method ${String(property)} returned:`, result);
          return result;
        };
      }
      return originalMethod;
    },
  });

  return proxy as T;
}

class Pocket {
  private static instance: Pocket | null = null;
  private cards: { [name: string]: ICard } = {};

  private constructor() {}

  public static getInstance(): Pocket {
    if (!Pocket.instance) {
      Pocket.instance = new Pocket();
    }
    return Pocket.instance;
  }

  public AddCard(name: string, card: ICard): void {
    this.cards[name] = card;
  }

  public RemoveCard(name: string): void {
    delete this.cards[name];
  }

  public GetCard(name: string): ICard | undefined {
    return this.cards[name];
  }

  public GetTotalAmount(currency: CurrencyEnum): number {
    let totalAmount = 0;
    for (const cardName in this.cards) {
      const card = this.cards[cardName];
      totalAmount += card.GetBalance(currency);
    }
    return totalAmount;
  }
}

const card1 = CardProxyLogger(new Card());
const card2 = CardProxyLogger(new BonusCard());

card1.AddTransaction(new Transaction(100, CurrencyEnum.USD));
card1.AddTransaction(new Transaction(200, CurrencyEnum.UAH));

card2.AddTransaction(new Transaction(50, CurrencyEnum.USD));
card2.AddTransaction(new Transaction(150, CurrencyEnum.UAH));

const pocket = Pocket.getInstance();
pocket.AddCard("Card 1", card1);
pocket.AddCard("Card 2", card2);

console.log("Card 1 balance in USD:", card1.GetBalance(CurrencyEnum.USD));
console.log("Card 2 balance in UAH:", card2.GetBalance(CurrencyEnum.UAH));
console.log("Total amount in USD:", pocket.GetTotalAmount(CurrencyEnum.USD));

const card1Memento = card1.SaveState();

card1.AddTransaction(new Transaction(50, CurrencyEnum.USD));
console.log("Card 1 balance after additional transaction:", card1.GetBalance(CurrencyEnum.USD));

card1.Revert(card1Memento);
console.log("Card 1 balance after revert:", card1.GetBalance(CurrencyEnum.USD));
